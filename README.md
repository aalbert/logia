# LogIA

This repository contains the code for the mini-projects of the 2022 session of the logIA module.

### Development environment

With nix installed, simply type `nix develop`. This provides an environment
with all the necessary tools and libraries, including dune, merlin, dolmen and minisat.

### Building 

With nix installed, simply type `nix build .#project1` to build project1, etc.
Otherwise, please follow the per-project building instructions.
