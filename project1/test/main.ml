open Lib

let create_file = fun horn nb_var nb_clause ->
  let oc = open_out "random.cnf" in
  generate_dimacs oc horn nb_var nb_clause;
  close_out oc;
  ()

let test_horn = fun _ ->
  create_file true 5 10;
  print_file "random.cnf";
  print_endline "---- Solver answer ----";
  let _ = Unix.system "project1 -v -m horn random.cnf" in
  print_endline "---- Minisat answer ---";
  let _ = Unix.system "minisat random.cnf output.txt" in
  let _ = Unix.system "cat output.txt" in
  ()

let test_xor = fun _ ->
  create_file false 6 10;
  print_file "random.cnf";
  print_endline "---- Solver answer ----";
  let _ = Unix.system "project1 -v -m xor random.cnf" in
  print_endline "---- Minisat answer ---";
  convert_xorsat "random.cnf";
  (*print_endline "converted random.cnf";*)
  (*let _ = Unix.system "cat conversion.cnf" in*)
  let _ = Unix.system "minisat conversion.cnf output.txt" in
  let _ = Unix.system "cat output.txt" in
  ()

let many_tests_horn = fun n ->
  for _ = 1 to n do
    create_file true 20 300;
    let _ = Unix.system "project1 -m horn random.cnf > output1.txt" in
    let _ = Unix.system "minisat -verb=0 random.cnf output2.txt > /dev/null" in
    let _ = Unix.system "diff output1.txt output2.txt" in
    ()
  done

let many_tests_xor = fun n ->
  for _ = 1 to n do
    create_file false 6 10 ;
    let _ = Unix.system "project1 -m xor random.cnf > output1.txt" in
    convert_xorsat "random.cnf";
    let _ = Unix.system "minisat -verb=0 conversion.cnf output2.txt > /dev/null" in
    let _ = Unix.system "diff output1.txt output2.txt" in
    ()
  done

let () =
  print_endline "---- Running random Horn test --";
  test_horn ();

  print_endline "---- Running random Xor test ---";
  test_xor ();

  print_endline "---- Running 20 Horn tests ---";
  many_tests_horn 20;

  print_endline "---- Running 20 Xor tests ----";
  many_tests_xor 20;
