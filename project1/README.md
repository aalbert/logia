# Mini project 1

This project includes XOR-SAT, Horn-SAT, a random formula generator and benchmark tests.

### Usage 

Building or executing project1 should be done through dune. Expected command line arguments are `project1 [--mode {xor|horn}] <files> ...`

To test a specific file: `dune exec -- project1 --mode xor file.cnf`

To run tests: `dune test --force`

Tests are done as follows:
- For Horn-SAT, the formula is given to Minisat to evaluate, the result is then compared to the one given by our algorithm
- for Xor-SAT, the formula is first converted from Xor to CNF before being passed down to Minisat for comparison.

From our testing, our Horn-SAT Algorithm works on 100% of our tests, Xor-SAT has a ~97% success rate, though the errors may be due to the XOR-to-CNF conversion, and not the Xor-SAT algorithm itself.


 
