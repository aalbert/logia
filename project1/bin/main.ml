open Printf
open Sat_result

(* Command line parsing configuration *)
let usage_msg = "project1 [--mode {xor|horn}] <files> ..."
let input_files = ref []
let verbose = ref false
let mode = ref false
let set_mode s = mode := String.equal "horn" s
let anon_fun filename =
  input_files := filename :: !input_files
let speclist =
  [("--mode", Arg.String set_mode, "Set processing mode (xor or horn), default to Xor");
   ("-m", Arg.String set_mode, "Set processing mode (xor or horn), default to Xor");
   ("--verbose", Arg.Set verbose, "Output debug information");
   ("-v", Arg.Set verbose, "Output debug information");]

let () =

  (* parsing command line argument *)
  Arg.parse speclist anon_fun usage_msg;

  (* start timer *)
  let time_begin = Sys.time () in

  if !mode then (

    (* parsing files *)
    let parsed_files = List.map Horn.parse_file !input_files in
    let time_parse = Sys.time () -. time_begin in

    (* processing parsed_files *)
    let results = List.map Horn.process parsed_files in
    let time_process = Sys.time () -. time_parse -. time_begin in

    (* printing results *)
    List.iter (print_result !verbose) results;
    if !verbose then printf "parsing: %fs, processing: %fs\n" time_parse time_process

  ) else (

    (* parsing files *)
    let parsed_files = List.map Xor.parse_file !input_files in
    let time_parse = Sys.time () -. time_begin in

    (* processing parsed_files *)
    let results = List.map Xor.process parsed_files in
    let time_process = Sys.time () -. time_parse -. time_begin in

    (* printing results *)
    List.iter (print_result !verbose) results;
    if !verbose then printf "parsing: %fs, processing: %fs\n" time_parse time_process
  )
