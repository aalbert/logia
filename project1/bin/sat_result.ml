open Printf

(* Program trace *)
type trace = string list
let print_trace = List.iter print_endline

(* SAT problem model *)
(* represented by the list of variable or their negation that are true *)
(* ordered by variable *)
(* ex: [1; -2; 3; -4; -5] *)
type model = int list
let print_model model =
  List.iter (printf "%d ") model;
  print_endline "0"

(* Result printing *)
let print_result verbose = function
  | Ok (model, trace) -> (
      if verbose then print_trace trace;
      print_endline "SAT";
      print_model model
    )
  | Error trace -> (
      if verbose then print_trace trace;
      print_endline "UNSAT"
    )
