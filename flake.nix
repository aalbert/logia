{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        ocamlPackages = pkgs.ocaml-ng.ocamlPackages_4_13;
	python = pkgs.python310;
        pythonPackages = pkgs.python310Packages;
        dolmen = ocamlPackages.dolmen.overrideAttrs (old: {
          src = pkgs.fetchurl {
            url =
              "https://github.com/Gbury/dolmen/releases/download/v0.7/dolmen-0.7.tbz";
            sha256 = "sha256-/yiJ+p1GfVtNh65PgZpkNYcV9FfMYia0VUY8L81Ksq8=";
          };
        });
      in rec {
        packages = {

          default = self.packages.${system}.project1;

          project1 = ocamlPackages.buildDunePackage {
            pname = "project1";
            version = "0.1.0";
            duneVersion = "2";
            minimalOcamlVersion = "4.13";
            src = ./project1/.;
            buildInputs = with ocamlPackages; [ dolmen ];
          };

        };

        devShells.default = pkgs.mkShell {
          name = "logIA-dev";
          packages = with ocamlPackages; [
	    python
	    z3
	    pythonPackages.z3
	    pythonPackages.setuptools
            ocaml
            findlib
            dune_2
            ocaml-lsp
            dolmen
            merlin
            pkgs.minisat
	    pkgs.swiProlog
          ];
        };
      });
}
