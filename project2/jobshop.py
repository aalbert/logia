from z3 import *

jobs = [
    [(1, 21), (0, 53), (4, 95), (3, 55), (2, 34)],
    [(0, 21), (3, 52), (4, 16), (2, 26), (1, 71)],
    [(3, 39), (4, 98), (1, 42), (2, 31), (0, 12)],
    [(1, 77), (0, 55), (4, 79), (2, 66), (3, 77)],
    [(0, 83), (3, 34), (2, 64), (1, 19), (4, 37)],
    [(1, 54), (2, 43), (4, 79), (0, 92), (3, 62)],
    [(3, 69), (4, 77), (1, 87), (2, 87), (0, 93)],
    [(2, 38), (0, 60), (1, 41), (3, 24), (4, 83)],
    [(3, 17), (1, 49), (4, 25), (0, 44), (2, 98)],
    [(4, 77), (3, 79), (2, 43), (1, 75), (0, 96)],
]

#jobs = [[(0, 3), (1, 2), (2, 2)], [(0, 2), (2, 1), (1, 4)], [(0, 4), (1, 4), (2, 3)]]

times = [[0 for _ in range(len(jobs[0]))] for _ in range(len(jobs))]
for job in range(len(jobs)):
    for (machine, time) in jobs[job]:
        times[job][machine] = time

schedule = [
    [
        Int("job %s, machine %s start time " % (job, machine))
        for machine in range(len(jobs[0]))
    ]
    for job in range(len(jobs))
]

end_time = Int("end time ")

opti = Optimize()

# jobs begin after t = 0s and end before t = end_time
for job in range(len(jobs)):
    for step in range(len(jobs[0])):
        machine, time = jobs[job][step]
        opti.add(schedule[job][machine] >= 0)
        opti.add(schedule[job][machine] + time <= end_time)

# each job respect it's given machine order
for job in range(len(jobs)):
    for step in range(len(jobs[0]) - 1):
        machine1, time = jobs[job][step]
        machine2, _ = jobs[job][step + 1]
        opti.add(schedule[job][machine2] >= schedule[job][machine1] + time)

# machine usage cannot overlap
for job1 in range(len(jobs)):
    for job2 in range(job1 + 1, len(jobs)):
        for machine in range(len(jobs[0])):
            time1 = times[job1][machine]
            time2 = times[job2][machine]
            opti.add(
                Or(
                    schedule[job1][machine] + time1 <= schedule[job2][machine],
                    schedule[job2][machine] + time2 <= schedule[job1][machine],
                )
            )

opti.minimize(end_time)

print(opti.check())
print(opti.model())
