# Mini project 2

This project includes: 

- an N jobs M machines instance of job-shop in Prolog and applications to the given (3,3) and (10,5) instances: `jobshop.pl`
- an N jobs M machines instance of job-shop in Z3 through python and applications to the given (3,3) and (10,5) instances: `jobshop.py`
- a model of the (3,3) instance in Z3: `jobshop3.smt2`
- a python script to generate Z3 code, but quite inneficient to solve problems: `gen_z3.py`

### Usage 
With nix installed, simply type `swipl jobshop.pl` to run job-shop related queries in Prolog and `python3 jobshop10.py` to run Z3 through the python interface. Prolog and Z3 can also be done online on <https://swish.swi-prolog.org/> and <https://compsys-tools.ens-lyon.fr/z3/> respectively.
