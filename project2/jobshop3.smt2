; Variable declarations
(declare-fun job00 () Int)
(declare-fun job01 () Int)
(declare-fun job02 () Int)

(declare-fun job10 () Int)
(declare-fun job11 () Int)
(declare-fun job12 () Int)

(declare-fun job20 () Int)
(declare-fun job21 () Int)
(declare-fun job22 () Int)

(assert (and
        (<= 0 job00)
        (<= 0 job01)
        (<= 0 job02)


        (<= 0 job10)
        (<= 0 job11)
        (<= 0 job12)

        (<= 0 job20)
        (<= 0 job21)
        (<= 0 job22)
    )
)


; No task for a job can be started until the previous task for that job is completed
(assert (<= (+ job00 3) job01))
(assert (<= (+ job01 2) job02))

(assert (<= (+ job10 2) job11))
(assert (<= (+ job11 1) job12))


(assert (<= (+ job20 4) job21))
(assert (<= (+ job21 4) job22))

; A machine can only work on one task at a time.

; Machine 0

(assert 
    (or
        (and (<= (+ job00 3) job10) (<= (+ job10 2) job20))
        (and (<= (+ job00 3) job20) (<= (+ job20 4) job10))

        (and (<= (+ job10 2) job00) (<= (+ job00 3) job20))
        (and (<= (+ job10 2) job20) (<= (+ job20 4) job00))
        
        (and (<= (+ job20 4) job00) (<= (+ job00 3) job10))
        (and (<= (+ job20 4) job10) (<= (+ job10 2) job00))        
    )
)

; Machine 1

(assert 
    (or
        (and (<= (+ job01 2) job12) (<= (+ job12 4) job21))
        (and (<= (+ job01 2) job21) (<= (+ job21 4) job12))

        (and (<= (+ job12 4) job01) (<= (+ job01 2) job21))
        (and (<= (+ job12 4) job21) (<= (+ job21 4) job01))
        
        (and (<= (+ job21 4) job01) (<= (+ job01 2) job12))
        (and (<= (+ job21 4) job12) (<= (+ job12 4) job01))        
    )
)

; Machine 2

(assert 
    (or
        (and (<= (+ job02 2) job11) (<= (+ job11 1) job22))
        (and (<= (+ job02 2) job22) (<= (+ job22 3) job11))

        (and (<= (+ job11 1) job02) (<= (+ job02 2) job22))
        (and (<= (+ job11 1) job22) (<= (+ job22 3) job02))
        
        (and (<= (+ job22 3) job02) (<= (+ job02 2) job11))
        (and (<= (+ job22 3) job11) (<= (+ job11 1) job02))        
    )
)

(define-fun max ((x Int) (y Int)) Int
  (ite (< x y) y x))

; Solve
(minimize (max (+ job02 2) (max (+ job12 4) (+ job22 3))))
(check-sat)
(get-model)
